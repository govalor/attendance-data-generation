﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaModel
{
    public class StudentRecord
    {
        public class ZnwWorkflowInstance
        {
        }

        public class AppData
        {
            public object nwId { get; set; }
            public string CMLastName { get; set; }
            public string CMFirstName { get; set; }
            public object CMMiddleName { get; set; }
            public string CMGradeLevel { get; set; }
            public string CMStudentEmail { get; set; }
            public string CMParents1 { get; set; }
            public string CMHomePhone1 { get; set; }
            public object CMParents2 { get; set; }
            public object CMHomePhone2 { get; set; }
            public string CMAddressBlock1 { get; set; }
            public string CMCityStatePostBlock1 { get; set; }
            public object CMAddressBlock2 { get; set; }
            public object CMCityStatePostBlock2 { get; set; }
            public object CMSiblings { get; set; }
            public string CMDisplayName { get; set; }
            public string CMStudentIDNumber { get; set; }
            public object nwExternalId { get; set; }
            public object nwCreatedByUser { get; set; }
            public object nwCreatedDate { get; set; }
            public object nwLastModifiedByUser { get; set; }
            public string nwLastModifiedDate { get; set; }
            public object nwTenantStripe { get; set; }
            public object AttachmentGroupId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public bool znwLocked { get; set; }
            public object znwSparseOwner { get; set; }
            public object nwNateId { get; set; }
            public object nwNateDisposition { get; set; }
            public object InternalSeq { get; set; }
            public string nwBaseVersion { get; set; }
        }

        public class Record
        {
            public AppData appData { get; set; }
            public string version { get; set; }
            public string nateDisposition { get; set; }
        }

        public class ClientState
        {
            public string RootNwId { get; set; }
            public string SaveType { get; set; }
            public string ApplicationName { get; set; }
        }

        public class RootObject
        {
            public List<Record> records { get; set; }
            public ClientState clientState { get; set; }
            public bool createContainer { get; set; }
            public bool commitContainer { get; set; }
        }
    }
}
