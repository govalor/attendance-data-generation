﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaModel
{
    public class CoursesNWPOST
    {
        public class ZnwWorkflowInstance
        {
        }

        public class AppData
        {
            public object nwId { get; set; }
            public int JVCycleDaysID { get; set; }
            public int JVEA7RecordsID { get; set; }
            public string JVBlockColor { get; set; }
            public string JVBlockColorRGB { get; set; }
            public string JVDay { get; set; }
            public string JVCourseSection { get; set; }
            public string JVFaculty { get; set; }
            public string JVSemester { get; set; }
            public string JVStartTime { get; set; }
            public string JVEndTime { get; set; }
            public int JVStartTimeRaw { get; set; }
            public int JVEndTimeRaw { get; set; }
            public string JVRoom { get; set; }
            public string JVCourse { get; set; }
            public string JVClassName { get; set; }
            public object nwExternalId { get; set; }
            public object nwCreatedByUser { get; set; }
            public object nwCreatedDate { get; set; }
            public object nwLastModifiedByUser { get; set; }
            public string nwLastModifiedDate { get; set; }
            public object nwTenantStripe { get; set; }
            public object AttachmentGroupId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public bool znwLocked { get; set; }
            public object znwSparseOwner { get; set; }
            public object nwNateId { get; set; }
            public object nwNateDisposition { get; set; }
            public object InternalSeq { get; set; }
            public string nwBaseVersion { get; set; }
        }

        public class Record
        {
            public AppData appData { get; set; }
            public string version { get; set; }
            public string nateDisposition { get; set; }
        }

        public class ClientState
        {
            public string RootNwId { get; set; }
            public string SaveType { get; set; }
            public string ApplicationName { get; set; }
        }

        public class RootObject
        {
            public List<Record> records { get; set; }
            public ClientState clientState { get; set; }
            public bool createContainer { get; set; }
            public bool commitContainer { get; set; }
        }
    }
}
