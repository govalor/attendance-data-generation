﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaModel
{
    public class Model
    {
        public class ZnwWorkflowInstance
        {
        }

        public class AppData
        {
            //List of classes, schedule
            public List<ClassModel> StudentSchedule { get; set; }
            public List<ClassModel> TeacherSchedule { get; set; }
            public List<StudentGradesModel> StudentGrades { get; set; }
            public List<FacultyAttendance.Record> AttendanceCourses { get; set; }

            //generated data items
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string NickName { get; set; }
            public string Title { get; set; }
            public string DisplayName { get; set; }
            public string GradeLevel { get; set; }
            public string Email { get; set; }
            public string Parents1 { get; set; }
            public string HomePhone1 { get; set; }
            public string Parents2 { get; set; }
            public string HomePhone2 { get; set; }
            public string AddressBlock1 { get; set; }
            public string CityStatePostBlock1 { get; set; }
            public string AddressBlock2 { get; set; }
            public string CityStatePostBlock2 { get; set; }
            public string Siblings { get; set; }
            public string StudentIDNumber { get; set; }
            public int ClassOf { get; set; }
            public int EA7RecordsID { get; set; }
            public string Gender { get; set; }
            public string Faculty { get; set; }
            public string Department { get; set; }
            public int FacultyID { get; set; }

            //for gmail
            public string SchoolIDForFacutly { get; set; }
            public string OrgUnitPathFaculty { get; set; }
            public string OrgUnitPathStudent { get; set; }
            public string GroupEmailFaculty { get; set; }
            public string GroupEmailStudent { get; set; }

            //for AD
            public string OUDefaultGroupS { get; set; }
            public string OUDefaultGroupFS { get; set; }
            public string OUClassGroupS { get; set; }
            public string OUClassGroupFS { get; set; }
            public string OULocation { get; set; }
            public string Password { get; set; }
            public string RecordType { get; set; }

            //NW Stuff - potentially delete
            public object nwId { get; set; }
            public object nwExternalId { get; set; }
            public object nwCreatedByUser { get; set; }
            public object nwCreatedDate { get; set; }
            public object nwLastModifiedByUser { get; set; }
            public string nwLastModifiedDate { get; set; }
            public object nwTenantStripe { get; set; }
            public object AttachmentGroupId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public bool znwLocked { get; set; }
            public object znwSparseOwner { get; set; }
            public object nwNateId { get; set; }
            public object nwNateDisposition { get; set; }
            public object InternalSeq { get; set; }
            public string nwBaseVersion { get; set; }
        }

        public class Record
        {
            public AppData appData { get; set; }
            public string version { get; set; }
            public string nateDisposition { get; set; }
        }

        public class ClientState
        {
            public string RootNwId { get; set; }
            public string SaveType { get; set; }
            public string ApplicationName { get; set; }
        }

        public class RootObject
        {
            public List<Record> records { get; set; }
            public ClientState clientState { get; set; }
            public bool createContainer { get; set; }
            public bool commitContainer { get; set; }
        }
    }

    public class StudentGradesModel
    {
        public int EA7RecordsID { get; set; }
        public string CourseSection { get; set; }
        public string SectionCode { get; set; }
        public string Faculty { get; set; }
        public string Room { get; set; }
        public string Course { get; set; }
        public string Classname { get; set; }
        public double Grade { get; set; }
    }

    public class ClassModel
    {
        public object nwId { get; set; }
        public int CycleDaysID { get; set; }
        public int EA7RecordsID { get; set; }
        public string BlockColor { get; set; }
        public string BlockColorRGB { get; set; }
        public string Day { get; set; }
        public string CourseSection { get; set; }
        public string Faculty { get; set; }
        public string Semester { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int StartTimeRaw { get; set; }
        public int EndTimeRaw { get; set; }
        public string Room { get; set; }
        public string Course { get; set; }
        public string ClassName { get; set; }
        public object nwExternalId { get; set; }
        public object nwCreatedByUser { get; set; }
        public object nwCreatedDate { get; set; }
        public object nwLastModifiedByUser { get; set; }
        public string nwLastModifiedDate { get; set; }
        public object nwTenantStripe { get; set; }
        public object AttachmentGroupId { get; set; }
        public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
        public object znwOwner { get; set; }
        public bool znwLocked { get; set; }
        public object znwSparseOwner { get; set; }
        public object nwNateId { get; set; }
        public object nwNateDisposition { get; set; }
        public object InternalSeq { get; set; }

        public class ZnwWorkflowInstance
        {
        }
    }
}
