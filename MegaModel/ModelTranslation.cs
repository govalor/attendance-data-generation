﻿using MegaModel.Translation_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaModel
{
    public class ModelTranslation
    {
        public ModelTranslation() {} //empty constructor

        //Post model for CMGetParentStudentDirectory NW App
        public StudentRecord.RootObject toParentDirRecord(Model.RootObject people)
        {
            StudentRecord.RootObject students = new StudentRecord.RootObject();
            List<StudentRecord.Record> studentList = new List<StudentRecord.Record>();
            StudentRecord.Record individualStudent;
            StudentRecord.AppData newStudent;
            Model.AppData oldRecord;

            for(int i = 0; i < people.records.Count(); i++)
            {
                //get person to be translated
                oldRecord = people.records.ElementAt(i).appData;
                newStudent = new StudentRecord.AppData();
                individualStudent = new StudentRecord.Record();

                //student information
                newStudent.CMFirstName = oldRecord.FirstName;
                newStudent.CMLastName = oldRecord.LastName;
                newStudent.CMMiddleName = oldRecord.MiddleName;
                newStudent.CMDisplayName = oldRecord.DisplayName;
                newStudent.CMStudentEmail = oldRecord.Email;
                newStudent.CMGradeLevel = oldRecord.GradeLevel + "th Grade";
                newStudent.CMStudentIDNumber = oldRecord.StudentIDNumber;

                //family information
                newStudent.CMParents1 = oldRecord.Parents1;
                newStudent.CMParents2 = oldRecord.Parents2;
                newStudent.CMSiblings = oldRecord.Siblings;
                newStudent.CMHomePhone1 = oldRecord.HomePhone1;
                newStudent.CMHomePhone2 = oldRecord.HomePhone2;
                newStudent.CMAddressBlock1 = oldRecord.AddressBlock1;
                newStudent.CMAddressBlock2 = oldRecord.AddressBlock2;
                newStudent.CMCityStatePostBlock1 = oldRecord.CityStatePostBlock1;
                newStudent.CMCityStatePostBlock2 = oldRecord.CityStatePostBlock2;

                individualStudent.appData = newStudent;
                studentList.Add(individualStudent);
            }

            students.records = studentList;
            return students;
        }

        //Post model for JVGetStudentResult NW App - creates student for Schoology Schedule application
        public StudentNWPOST.RootObject toStudentRecord(Model.RootObject people)
        {
            StudentNWPOST.RootObject students = new StudentNWPOST.RootObject();
            List<StudentNWPOST.Record> studentList = new List<StudentNWPOST.Record>();
            StudentNWPOST.Record individualStudent;
            StudentNWPOST.AppData newStudent;
            Model.AppData oldRecord;

            for (int i = 0; i < people.records.Count(); i++)
            {
                //get student to be translated
                newStudent = new StudentNWPOST.AppData();
                individualStudent = new StudentNWPOST.Record();
                oldRecord = people.records.ElementAt(i).appData;

                //assign attributes
                newStudent.JVClassOf = oldRecord.ClassOf;
                newStudent.JVEA7RecordsID = oldRecord.EA7RecordsID;
                newStudent.JVEmail = oldRecord.Email;
                newStudent.JVGender = oldRecord.Gender;
                newStudent.JVGradeLevel = oldRecord.GradeLevel;
                newStudent.JVLastName = oldRecord.LastName;
                newStudent.JVMiddleName = oldRecord.MiddleName;
                newStudent.JVFirstName = oldRecord.FirstName;
                newStudent.JVNickName = oldRecord.NickName;
                newStudent.JVTitle = oldRecord.Title;
                newStudent.JVUserDefinedID = oldRecord.StudentIDNumber;

                individualStudent.appData = newStudent;
                studentList.Add(individualStudent);
            }

            students.records = studentList;

            return students;
        }

        //Post model for JVGetFacultyResult NW App - creates a faculty member used with Schoology Schedule application
        public TeacherNWPOST.RootObject toFacultyRecord(Model.RootObject people)
        {
            TeacherNWPOST.RootObject faculty = new TeacherNWPOST.RootObject();
            List<TeacherNWPOST.Record> facultyList = new List<TeacherNWPOST.Record>();
            TeacherNWPOST.Record individualFaculty;
            TeacherNWPOST.AppData newFaculty;
            Model.AppData oldRecord;

            for(int i = 0; i < people.records.Count(); i++)
            {
                //get mega model person
                newFaculty = new TeacherNWPOST.AppData();
                individualFaculty = new TeacherNWPOST.Record();
                oldRecord = people.records.ElementAt(i).appData;

                //translate to appropriate model
                newFaculty.JVEA7RecordsID = oldRecord.EA7RecordsID;
                newFaculty.JVFirstName = oldRecord.FirstName;
                newFaculty.JVLastName = oldRecord.LastName;
                newFaculty.JVSchoolIDForFacutly = oldRecord.SchoolIDForFacutly;
                newFaculty.JVFacultyID = oldRecord.FacultyID;
                newFaculty.EmailAddress = oldRecord.Email;
                newFaculty.JVDepartment = oldRecord.Department;

                individualFaculty.appData = newFaculty;
                facultyList.Add(individualFaculty);
            }

            faculty.records = facultyList;
            return faculty;
        }

        /*Post model for JVGetStudentSchedule NW App
         * 
         * Changes list of people's schedule from the mega model
        */
        public List<CoursesNWPOST.RootObject> toStudentSchedule(Model.RootObject people)
        {
            //old schedule
            List<Model.Record> peopleRecords = people.records;
            Model.AppData pplAppData;
            List<ClassModel> listOfClasses;
            ClassModel oldClass;

            //new schedule
            List<CoursesNWPOST.RootObject> listofSchedules = new List<CoursesNWPOST.RootObject>();
            CoursesNWPOST.RootObject schedule = new CoursesNWPOST.RootObject();
            List<CoursesNWPOST.Record> listOfNewClasses = new List<CoursesNWPOST.Record>();
            CoursesNWPOST.Record newClass;
            CoursesNWPOST.AppData classData;
            
           for(int i = 0; i < people.records.Count(); i++)
           {
                pplAppData = peopleRecords.ElementAt(i).appData;
                listOfClasses = pplAppData.StudentSchedule;
                
                listOfNewClasses = new List<CoursesNWPOST.Record>();
                schedule = new CoursesNWPOST.RootObject();

                for (int j = 0; j < listOfClasses.Count(); j++)
                {
                    //get old class
                    oldClass = listOfClasses.ElementAt(j);

                    newClass = new CoursesNWPOST.Record();
                    classData = new CoursesNWPOST.AppData();
                    
                    classData.JVCycleDaysID = oldClass.CycleDaysID;
                    classData.JVEA7RecordsID = oldClass.EA7RecordsID;
                    classData.JVBlockColor = oldClass.BlockColor;
                    classData.JVBlockColorRGB = oldClass.BlockColorRGB;
                    classData.JVDay = oldClass.Day;
                    classData.JVCourseSection = oldClass.CourseSection;
                    classData.JVFaculty = oldClass.Faculty;
                    classData.JVSemester = oldClass.Semester;
                    classData.JVStartTime = oldClass.StartTime;
                    classData.JVEndTime = oldClass.EndTime;
                    classData.JVStartTimeRaw = oldClass.StartTimeRaw;
                    classData.JVEndTimeRaw = oldClass.EndTimeRaw;
                    classData.JVRoom = oldClass.Room;
                    classData.JVCourse = oldClass.Course;
                    classData.JVClassName = oldClass.ClassName;

                    //add individual class to new schedule
                    newClass.appData = classData;
                    listOfNewClasses.Add(newClass);
                }
                
                //add individual schedule to list of schedules
                schedule.records = listOfNewClasses;
                listofSchedules.Add(schedule);
            }
           
            return listofSchedules;
        }

        //changes the StudentGrades model into CMStudentGradesModel used for Eligibility Reports
        public List<CMStudentGradesModel.RootObject> toEligibilityReport(Model.RootObject peopleList)
        {
            List<CMStudentGradesModel.RootObject> reports = new List<CMStudentGradesModel.RootObject>();
            List<StudentGradesModel> oldRecord;
            StudentGradesModel oldClass;

            //make these new each time
            CMStudentGradesModel.RootObject reportRoot;
            List<CMStudentGradesModel.Record> classReports;
            CMStudentGradesModel.Record oneReport;
            CMStudentGradesModel.AppData oneAppdata;
           // CMStudentGradesModel.AppData reportAppData; //see if can get away w/o this

            for(int i = 0; i < peopleList.records.Count(); i++)
            {
                oldRecord = peopleList.records.ElementAt(i).appData.StudentGrades;
                classReports = new List<CMStudentGradesModel.Record>();
                reportRoot = new CMStudentGradesModel.RootObject();

                for (int j = 0; j < oldRecord.Count(); j++)
                {
                    oneReport = new CMStudentGradesModel.Record();
                    oneAppdata = new CMStudentGradesModel.AppData();
                    oldClass = oldRecord.ElementAt(j);

                    oneAppdata.CMEA7RecordsID = oldClass.EA7RecordsID;
                    oneAppdata.CMStudentIDNumber = peopleList.records.ElementAt(i).appData.StudentIDNumber;
                    oneAppdata.CMFirstName = peopleList.records.ElementAt(i).appData.FirstName;
                    oneAppdata.CMLastName = peopleList.records.ElementAt(i).appData.LastName;
                    oneAppdata.CMGradeLevel = peopleList.records.ElementAt(i).appData.GradeLevel + "th Grade";
                    oneAppdata.CMSectionCode = oldClass.SectionCode;
                    oneAppdata.CMCourseSection = oldClass.CourseSection;
                    oneAppdata.CMFaculty = oldClass.Faculty;
                    oneAppdata.CMRoom = oldClass.Room;
                    oneAppdata.CMCourse = oldClass.Course;
                    oneAppdata.CMClassName = oldClass.Classname;
                    oneAppdata.CMGrade = oldClass.Grade;

                    //class to report
                    oneReport.appData = oneAppdata;
                    classReports.Add(oneReport);
                }

                //add report to list of reports
                reportRoot.records = classReports;
                reports.Add(reportRoot);
            }

            return reports;
        }

        //Post model for JVGetFacultySchedule NW App
        public List<TeacherScheduleNWPOST.RootObject> toFacultySchedule(Model.RootObject people)
        {
            //old schedule
            List<Model.Record> peopleRecords = people.records;
            Model.AppData pplAppData;
            List<ClassModel> listOfClasses;
            ClassModel oldClass;

            //new schedule
            List<TeacherScheduleNWPOST.RootObject> listofSchedules = new List<TeacherScheduleNWPOST.RootObject>();
            TeacherScheduleNWPOST.RootObject schedule = new TeacherScheduleNWPOST.RootObject();
            List<TeacherScheduleNWPOST.Record> listOfNewClasses;
            TeacherScheduleNWPOST.Record newClass;
            TeacherScheduleNWPOST.AppData classData;


            for (int i = 0; i < people.records.Count(); i++)
            {
                pplAppData = peopleRecords.ElementAt(i).appData;
                listOfClasses = pplAppData.TeacherSchedule;

                listOfNewClasses = new List<TeacherScheduleNWPOST.Record>();
                schedule = new TeacherScheduleNWPOST.RootObject();

                for (int j = 0; j < listOfClasses.Count(); j++)
                {
                    //get old class
                    oldClass = listOfClasses.ElementAt(j);

                    newClass = new TeacherScheduleNWPOST.Record();
                    classData = new TeacherScheduleNWPOST.AppData();
                    classData.JVCycleDaysID = oldClass.CycleDaysID;
                    classData.JVEA7RecordsID = oldClass.EA7RecordsID;
                    classData.JVBlockColor = oldClass.BlockColor;
                    classData.JVBlockColorRGB = oldClass.BlockColorRGB;
                    classData.JVDay = oldClass.Day;
                    classData.JVCourseSection = oldClass.CourseSection;
                    classData.JVFaculty = oldClass.Faculty;
                    classData.JVSemester = oldClass.Semester;
                    classData.JVStartTime = oldClass.StartTime;
                    classData.JVEndTime = oldClass.EndTime;
                    classData.JVStartTimeRaw = oldClass.StartTimeRaw;
                    classData.JVEndTimeRaw = oldClass.EndTimeRaw;
                    classData.JVRoom = oldClass.Room;
                    classData.JVCourse = oldClass.Course;
                    classData.JVClassName = oldClass.ClassName;

                    //add individual class to new schedule
                    newClass.appData = classData;
                    listOfNewClasses.Add(newClass);
                }

                //add individual schedule to list of schedules
                schedule.records = listOfNewClasses;
                listofSchedules.Add(schedule);
            }
            return listofSchedules;
        }

        public List<FacultyStaffModel.Record> toADFacultyStaff(Model.RootObject people)
        {
            //set up for new faculty list
            List<FacultyStaffModel.Record> facultyList = new List<FacultyStaffModel.Record>();
            List<Model.Record> peopleRecords = people.records;
            FacultyStaffModel.Record facultyRecord;
            FacultyStaffModel.AppData faculty;
            Model.AppData oldFaculty;

            for (int i = 0; i < peopleRecords.Count(); i++)
            {
                faculty = new FacultyStaffModel.AppData();
                facultyRecord = new FacultyStaffModel.Record();
                oldFaculty = peopleRecords.ElementAt(i).appData;

                faculty.CMEA7RecordsID = oldFaculty.EA7RecordsID;
                faculty.CMFacultyID = oldFaculty.SchoolIDForFacutly;
                faculty.CMNickName = oldFaculty.NickName;
                faculty.CMFirstName = oldFaculty.FirstName;
                faculty.CMLastName = oldFaculty.LastName;
                faculty.CMGender = oldFaculty.Gender;
                faculty.CMOUDefaultGroup = oldFaculty.OUDefaultGroupFS;
                faculty.CMOUClassGroup = oldFaculty.OUClassGroupFS;
                faculty.CMOULocation = oldFaculty.OULocation;
                faculty.CMEmail = oldFaculty.Email;
                faculty.CMPassword = oldFaculty.Password;

                facultyRecord.appData = faculty;
                facultyList.Add(facultyRecord);
            }

            return facultyList;
        }

        public List<StudentModel.Record> toADStudentList(Model.RootObject people)
        {
            List<StudentModel.Record> studentList = new List<StudentModel.Record>();
            List<Model.Record> peopleRecords = people.records;
            StudentModel.Record studentRecord;
            StudentModel.AppData student;
            Model.AppData oldStudent;

            for(int i = 0; i < peopleRecords.Count(); i++)
            {
                student = new StudentModel.AppData();
                studentRecord = new StudentModel.Record();
                oldStudent = peopleRecords.ElementAt(i).appData;

                student.CMEA7RecordsID = oldStudent.EA7RecordsID;
                student.CMStudentIDNumber = oldStudent.StudentIDNumber;
                student.CMNickName = oldStudent.NickName;
                student.CMTitle = oldStudent.Title;
                student.CMFirstName = oldStudent.FirstName;
                student.CMMiddleName = oldStudent.MiddleName;
                student.CMLastName = oldStudent.LastName;
                student.CMGender = oldStudent.Gender;
                student.CMGradeLevel = oldStudent.GradeLevel;
                student.CMOUDefaultGroup = oldStudent.OUDefaultGroupS;
                student.CMOUClassGroup = oldStudent.OUClassGroupS;
                student.CMOULocation = oldStudent.OULocation;
                student.CMEmail = oldStudent.Email;
                student.CMPassword = oldStudent.Password;

                studentRecord.appData = student;
                studentList.Add(studentRecord);
            }
            
            return studentList;
        }

        public List<ADUserModel> toADUserList(Model.RootObject people)
        {
            List<ADUserModel> adUsers = new List<ADUserModel>();
            Model.AppData oldUser;
            ADUserModel newUser;

            for(int i = 0; i < people.records.Count(); i++)
            {
                oldUser = people.records.ElementAt(i).appData;
                newUser = new ADUserModel();

                newUser.EA7RecordsID = oldUser.EA7RecordsID;
                newUser.NickName = oldUser.NickName;
                newUser.FirstName = oldUser.FirstName;
                newUser.LastName = oldUser.LastName;
                newUser.OULocation = oldUser.OULocation;
                newUser.OUClassGroup = oldUser.OUClassGroupFS; //CHECK!!
                newUser.OUDefaultGroup = oldUser.OUDefaultGroupFS; //CHECK!!
                newUser.Email = oldUser.Email;
                newUser.Password = oldUser.Password;

                adUsers.Add(newUser);
            }

            return adUsers;
        }

        public GoogleAdminStudent.RootObject toGoogleStudent(Model.RootObject people)
        {
            GoogleAdminStudent.RootObject students = new GoogleAdminStudent.RootObject();
            List<GoogleAdminStudent.Record> studentList = new List<GoogleAdminStudent.Record>();
            GoogleAdminStudent.Record individualStudent;
            GoogleAdminStudent.AppData newStudent;
            Model.AppData oldRecord;

            for (int i = 0; i < people.records.Count(); i++)
            {
                //get student to be translated
                newStudent = new GoogleAdminStudent.AppData();
                individualStudent = new GoogleAdminStudent.Record();
                oldRecord = people.records.ElementAt(i).appData;

                //assign attributes
                newStudent.JVEA7RecordsID = oldRecord.EA7RecordsID;
                newStudent.JVEmail = oldRecord.Email;
                newStudent.JVGender = oldRecord.Gender;
                newStudent.JVGradeLevel = oldRecord.GradeLevel;
                newStudent.JVLastName = oldRecord.LastName;
                newStudent.JVMiddleName = oldRecord.MiddleName;
                newStudent.JVFirstName = oldRecord.FirstName;
                newStudent.JVNickName = oldRecord.NickName;
                newStudent.JVTitle = oldRecord.Title;
                newStudent.JVUserDefinedID = oldRecord.StudentIDNumber;
                newStudent.JVOrgUnitPath = oldRecord.OrgUnitPathStudent;
                newStudent.JVGroupEmail = oldRecord.GroupEmailStudent;
                newStudent.JVPassword = oldRecord.Password;

                individualStudent.appData = newStudent;
                studentList.Add(individualStudent);
            }

            students.records = studentList;

            return students;
        }

        public GoogleAdminTeacher.RootObject toGoogleTeacher(Model.RootObject people)
        {
            GoogleAdminTeacher.RootObject teachers = new GoogleAdminTeacher.RootObject();
            List<GoogleAdminTeacher.Record> teacherList = new List<GoogleAdminTeacher.Record>();
            GoogleAdminTeacher.Record individualTeacher;
            GoogleAdminTeacher.AppData newTeacher;
            Model.AppData oldRecord;

            for (int i = 0; i < people.records.Count(); i++)
            {
                //get student to be translated
                newTeacher = new GoogleAdminTeacher.AppData();
                individualTeacher = new GoogleAdminTeacher.Record();
                oldRecord = people.records.ElementAt(i).appData;

                //assign attributes
                newTeacher.JVEA7RecordsID = oldRecord.EA7RecordsID;
                newTeacher.JVSchoolIDForFacutly = oldRecord.SchoolIDForFacutly;
                newTeacher.JVFirstName = oldRecord.FirstName;
                newTeacher.JVLastName = oldRecord.LastName;
                newTeacher.JVEmail = oldRecord.Email;
                newTeacher.JVPassword = oldRecord.Password;
                newTeacher.JVOrgUnitPath = oldRecord.OrgUnitPathFaculty;
                newTeacher.JVGroupEmail = oldRecord.GroupEmailFaculty;

                individualTeacher.appData = newTeacher;
                teacherList.Add(individualTeacher);
            }

            teachers.records = teacherList;

            return teachers;
        }

        public void printGoogleTeacher(GoogleAdminTeacher.RootObject teachers)
        {
            GoogleAdminTeacher.AppData data;

            for (int i = 0; i < teachers.records.Count(); i++)
            {
                data = teachers.records.ElementAt(i).appData;
                Console.WriteLine("Name: " + data.JVFirstName + " " + data.JVLastName);
                Console.WriteLine("Faculty ID: " + data.JVSchoolIDForFacutly);
                Console.WriteLine("OrgUnitPath and Group Email: " + data.JVOrgUnitPath + " " + data.JVGroupEmail);
            }
        }

        public void printGoogleStudent(GoogleAdminStudent.RootObject students)
        {
            GoogleAdminStudent.AppData data;

            for(int i = 0; i < students.records.Count(); i++)
            {
                data = students.records.ElementAt(i).appData;
                Console.WriteLine("Name: " + data.JVFirstName + " " + data.JVLastName);
                Console.WriteLine("Student ID: " + data.JVUserDefinedID);
                Console.WriteLine("OrgUnitPath and Group Email: " + data.JVOrgUnitPath + " " + data.JVGroupEmail);
            }
        }

        public void printADUsers(List<ADUserModel> list)
        {
            for(int i = 0; i < list.Count(); i++)
            {
                Console.WriteLine("Name: " + list.ElementAt(i).FirstName + " " + list.ElementAt(i).LastName);
                Console.WriteLine("Email: " + list.ElementAt(i).Email);
            }
        }

        public void printStudentADList(List<StudentModel.Record> list)
        {
            for(int i = 0; i < list.Count(); i++)
            {
                Console.WriteLine("Name: " + list.ElementAt(i).appData.CMFirstName + " " + list.ElementAt(i).appData.CMLastName);
                Console.WriteLine("OUDefalut Group: " + list.ElementAt(i).appData.CMOUDefaultGroup);
                Console.WriteLine("OUClass Group: " + list.ElementAt(i).appData.CMOUClassGroup);
                Console.WriteLine("Email: " + list.ElementAt(i).appData.CMEmail);
                Console.WriteLine();
            }
        }

        public void printFacultyStaff(List<FacultyStaffModel.Record> list)
        {
            FacultyStaffModel.Record faculty;
            for(int i = 0; i < list.Count(); i++)
            {
                faculty = list.ElementAt(i);
                Console.WriteLine("Name: " + faculty.appData.CMFirstName + " " + faculty.appData.CMLastName);
                Console.WriteLine("OUDefault Group: " + faculty.appData.CMOUDefaultGroup);
                Console.WriteLine("Email: " + faculty.appData.CMEmail);
            }
        }

        public void printStudentSchedule(List<CoursesNWPOST.RootObject> rootOfSchedules)
        {
            CoursesNWPOST.RootObject oneSchedule;
            CoursesNWPOST.AppData oneClass;

            for(int i = 0; i < rootOfSchedules.Count(); i++) //for each schedule
            {
                //get one schedule at a time
                oneSchedule = rootOfSchedules.ElementAt(i);
                Console.WriteLine("------------------Next Schedule-----------------");

                for (int j = 0; j < oneSchedule.records.Count(); j++) //for each class in the schedule
                {
                    //get one class at a time
                    oneClass = oneSchedule.records.ElementAt(j).appData;

                    //print out the class' attributes
                    Console.WriteLine();
                    Console.WriteLine("CycleDaysID: " + oneClass.JVCycleDaysID);
                    Console.WriteLine("EA7 ID: " + oneClass.JVEA7RecordsID);
                    Console.WriteLine("Block Color: " + oneClass.JVBlockColor);
                    Console.WriteLine("BlockColor RGB: " + oneClass.JVBlockColorRGB);
                    Console.WriteLine("Day: " + oneClass.JVDay);
                    Console.WriteLine("Course Section: " + oneClass.JVCourseSection);
                    Console.WriteLine("Faculty: " + oneClass.JVFaculty);
                    Console.WriteLine("Semester: " + oneClass.JVSemester);
                    Console.WriteLine("Start Time: " + oneClass.JVStartTime);
                    Console.WriteLine("End Time: " + oneClass.JVEndTime);
                    Console.WriteLine("Start Time Raw: " + oneClass.JVStartTimeRaw);
                    Console.WriteLine("End Time Raw: " + oneClass.JVEndTimeRaw);
                    Console.WriteLine("Room: " + oneClass.JVRoom);
                    Console.WriteLine("Course: " + oneClass.JVCourse);
                    Console.WriteLine("Class name: " + oneClass.JVClassName);
                    Console.WriteLine();
                }
            }
        }

        public void printSScheduleOld(Model.RootObject root)
        {
            List<Model.Record> people = root.records;
            List<ClassModel> classes;
            ClassModel aClass;

            for (int i = 0; i < people.Count(); i++)
            {
                classes = people.ElementAt(i).appData.StudentSchedule;
                Console.WriteLine("--------------Next Student---------------");

                for (int j = 0; j < classes.Count(); j++)
                {
                    aClass = classes.ElementAt(j);

                    Console.WriteLine("CycleDaysID: " + aClass.CycleDaysID);
                    Console.WriteLine("EA7 ID: " + aClass.EA7RecordsID);
                    Console.WriteLine("Block Color: " + aClass.BlockColor);
                    Console.WriteLine("BlockColor RGB: " + aClass.BlockColorRGB);
                    Console.WriteLine("Day: " + aClass.Day);
                    Console.WriteLine("Course Section: " + aClass.CourseSection);
                    Console.WriteLine("Faculty: " + aClass.Faculty);
                    Console.WriteLine("Semester: " + aClass.Semester);
                    Console.WriteLine("Start Time: " + aClass.StartTime);
                    Console.WriteLine("End Time: " + aClass.EndTime);
                    Console.WriteLine("Start Time Raw: " + aClass.StartTimeRaw);
                    Console.WriteLine("End Time Raw: " + aClass.EndTimeRaw);
                    Console.WriteLine("Room: " + aClass.Room);
                    Console.WriteLine("Course: " + aClass.Course);
                    Console.WriteLine("Class name: " + aClass.ClassName);
                    Console.WriteLine();
                    Console.WriteLine("-------------------Next Class----------------");
                    Console.WriteLine();
                }
            }
        }

        public void printFacultySchedule(List<TeacherScheduleNWPOST.RootObject> rootOfSchedules)
        {
            TeacherScheduleNWPOST.RootObject oneSchedule;
            TeacherScheduleNWPOST.AppData oneClass;

            for (int i = 0; i < rootOfSchedules.Count(); i++) //for each schedule
            {
                //get one schedule at a time
                oneSchedule = rootOfSchedules.ElementAt(i);

                for (int j = 0; j < oneSchedule.records.Count(); j++) //for each class in the schedule
                {
                    //get one class at a time
                    oneClass = oneSchedule.records.ElementAt(j).appData;

                    //print out the class' attributes
                    Console.WriteLine("------------------Next Class-----------------");
                    Console.WriteLine();
                    Console.WriteLine("CycleDaysID: " + oneClass.JVCycleDaysID);
                    Console.WriteLine("EA7 ID: " + oneClass.JVEA7RecordsID);
                    Console.WriteLine("Block Color: " + oneClass.JVBlockColor);
                    Console.WriteLine("BlockColor RGB: " + oneClass.JVBlockColorRGB);
                    Console.WriteLine("Day: " + oneClass.JVDay);
                    Console.WriteLine("Course Section: " + oneClass.JVCourseSection);
                    Console.WriteLine("Faculty: " + oneClass.JVFaculty);
                    Console.WriteLine("Semester: " + oneClass.JVSemester);
                    Console.WriteLine("Start Time: " + oneClass.JVStartTime);
                    Console.WriteLine("End Time: " + oneClass.JVEndTime);
                    Console.WriteLine("Start Time Raw: " + oneClass.JVStartTimeRaw);
                    Console.WriteLine("End Time Raw: " + oneClass.JVEndTimeRaw);
                    Console.WriteLine("Room: " + oneClass.JVRoom);
                    Console.WriteLine("Course: " + oneClass.JVCourse);
                    Console.WriteLine("Class name: " + oneClass.JVClassName);
                    Console.WriteLine();
                }
            }
        }

        public void printEligibilityReport(List<CMStudentGradesModel.RootObject> rootOfGrades)
        {
            CMStudentGradesModel.RootObject root;
            CMStudentGradesModel.AppData report;

            for (int i = 0; i < rootOfGrades.Count(); i++)
            {
                root = rootOfGrades.ElementAt(i);
                Console.WriteLine("-------------- Report #" + i + " --------------");

                for (int j = 0; j < root.records.Count(); j++)
                {
                    report = root.records.ElementAt(j).appData;

                    Console.WriteLine("EA7ID: " + report.CMEA7RecordsID);
                    Console.WriteLine("StudentID: " + report.CMStudentIDNumber);
                    Console.WriteLine("First Name: " + report.CMFirstName);
                    Console.WriteLine("Last Name: " + report.CMLastName);
                    Console.WriteLine("Grade Level: " + report.CMGradeLevel);
                    Console.WriteLine("Section Code: " + report.CMSectionCode);
                    Console.WriteLine("Course Section: " + report.CMCourseSection);
                    Console.WriteLine("Faculty: " + report.CMFaculty);
                    Console.WriteLine("Course: " + report.CMCourse);
                    Console.WriteLine("Class Name: " + report.CMClassName);
                    Console.WriteLine("Grade: " + report.CMGrade);

                    Console.WriteLine();
                    Console.WriteLine();
                }
            }
        }
    }
}
