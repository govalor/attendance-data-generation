﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandomNameGeneratorLibrary;

namespace MegaModel
{
    public class GenerateRecord
    { 
        public GenerateRecord(){}

        //public Model.RootObject getModel(PersonNameGenerator nameGen, PlaceNameGenerator placeGen, Random numGen, int numRecords)
        public Model.RootObject getModel(int numRecords)
        {
            //create generators
            PersonNameGenerator nameGen = new PersonNameGenerator();
            PlaceNameGenerator placeGen = new PlaceNameGenerator();
            Random numGen = new Random();

            //create empty model
            Model.RootObject root = new Model.RootObject();
            List<Model.Record> records = new List<Model.Record>();
            Model.Record tempRecord;
            Model.AppData appData;

            //for schedule and grades
            List<ClassModel> studentSchedule;
            List<ClassModel> teacherSchedule;
            List<StudentGradesModel> studentGrades;
            List<FacultyAttendance.Record> attendanceCourses = getAttendanceCourses();
            

            for(int i = 0; i < numRecords; i++)
            {
                //get personal attributes, including names, parent information, and ID numbers
                appData = new Model.AppData();
                tempRecord = new Model.Record();
                studentSchedule = new List<ClassModel>();
                teacherSchedule = new List<ClassModel>();
                studentGrades = new List<StudentGradesModel>();


                appData = getPerson(nameGen, placeGen, numGen, i + 1);

                //generate course and faculty information
                studentSchedule = getStudentSchedule(i + 1); //generate student schedule
                teacherSchedule = getTeacherSchedule(i + 1); //generate teacher schedule
                
                //add the class schedule to the person record
                appData.StudentSchedule = studentSchedule;
                appData.TeacherSchedule = teacherSchedule;

                //generate the student grades
                studentGrades = getStudentGrades();
                appData.StudentGrades = studentGrades;

                //add attendance
                appData.AttendanceCourses = attendanceCourses;

                tempRecord.appData = appData;
                records.Add(tempRecord);
                
            }
            root.records = records;

            return root;
        }

        public List<ClassModel> getTeacherSchedule(int i)
        {
            List<ClassModel> schedule = new List<ClassModel>();

            ClassModel yellowclass1 = new ClassModel();

            yellowclass1.CycleDaysID = 11;
            yellowclass1.BlockColor = "Yellow";
            yellowclass1.BlockColorRGB = "ffff00";
            yellowclass1.Day = "Monday";
            yellowclass1.CourseSection = "SPA 230 - 2";
            yellowclass1.Faculty = "Ms. Gina Smith";
            yellowclass1.Semester = "Spring";
            yellowclass1.StartTime = "9:30 am";
            yellowclass1.EndTime = "10:20 AM";
            yellowclass1.StartTimeRaw = 9 * 60 + 30;
            yellowclass1.EndTimeRaw = 10 * 60 + 20;
            yellowclass1.Room = "Room 025";
            yellowclass1.Course = "SPA 230";
            yellowclass1.ClassName = "Spanish Intermediate I";
            yellowclass1.EA7RecordsID = i;

            schedule.Add(yellowclass1);

            ClassModel greenclass1 = new ClassModel();

            greenclass1.CycleDaysID = 11;
            greenclass1.BlockColor = "Green";
            greenclass1.BlockColorRGB = "080000";
            greenclass1.Day = "Monday";
            greenclass1.CourseSection = "TEC 410 - 1";
            greenclass1.Faculty = "Mr. Darin Pearson";
            greenclass1.Semester = "Spring";
            greenclass1.StartTime = "10:30 AM";
            greenclass1.EndTime = "11:20 AM";
            greenclass1.StartTimeRaw = (int)new DateTime().Ticks;
            greenclass1.EndTimeRaw = (int)new DateTime().Ticks;
            greenclass1.Room = "Room VC 019";
            greenclass1.Course = "TEC 410";
            greenclass1.ClassName = "AP Computer Science A";
            greenclass1.EA7RecordsID = i;

            schedule.Add(greenclass1);

            return schedule;
        }

        public List<ClassModel> getStudentSchedule(int i)
        {
            List<ClassModel> schedule = new List<ClassModel>();

            ClassModel redclass1 = new ClassModel();
            
            redclass1.CycleDaysID = 11;
            redclass1.BlockColor = "Red";
            redclass1.BlockColorRGB = "ff0000";
            redclass1.Day = "Monday";
            redclass1.CourseSection = "PRT - 1";
            redclass1.Faculty = "";
            redclass1.Semester = "Spring";
            redclass1.StartTime = "7:40 am";
            redclass1.StartTimeRaw = 40;
            redclass1.EndTime = "8:30 am";
            redclass1.EndTimeRaw = 30;
            redclass1.Room = "";
            redclass1.Course = "PRT";
            redclass1.ClassName = "PRT";
            redclass1.EA7RecordsID = i;

            schedule.Add(redclass1);

            ClassModel orangeclass1 = new ClassModel();

            orangeclass1.CycleDaysID = 11;
            orangeclass1.BlockColor = "Orange";
            orangeclass1.BlockColorRGB = "ffa500";
            orangeclass1.Day = "Monday";
            orangeclass1.CourseSection = "PRT - 2";
            orangeclass1.Faculty = "";
            orangeclass1.Semester = "Spring";
            orangeclass1.StartTime = "8:35 am";
            orangeclass1.StartTimeRaw = 35;
            orangeclass1.EndTime = "9:25 am";
            orangeclass1.EndTimeRaw = 25;
            orangeclass1.Room = "";
            orangeclass1.Course = "PRT";
            orangeclass1.ClassName = "PRT";
            orangeclass1.EA7RecordsID = i;


            schedule.Add(orangeclass1);

            ClassModel yellowclass1 = new ClassModel();

            yellowclass1.CycleDaysID = 11;
            yellowclass1.BlockColor = "Yellow";
            yellowclass1.BlockColorRGB = "ffff00";
            yellowclass1.Day = "Monday";
            yellowclass1.CourseSection = "MAT 520";
            yellowclass1.Faculty = "Mr. Gabe Anderson";
            yellowclass1.Semester = "Spring";
            yellowclass1.StartTime = "9:30 am";
            yellowclass1.StartTimeRaw = 30;
            yellowclass1.EndTime = "10:20 am";
            yellowclass1.EndTimeRaw = 20;
            yellowclass1.Room = "Room 322";
            yellowclass1.Course = "MAT 520";
            yellowclass1.ClassName = "AP Calculus A/B";
            yellowclass1.EA7RecordsID = i;

            schedule.Add(yellowclass1);

            ClassModel greenclass1 = new ClassModel();

            greenclass1.CycleDaysID = 11;
            greenclass1.BlockColor = "Green";
            greenclass1.BlockColorRGB = "8000";
            greenclass1.Day = "Monday";
            greenclass1.CourseSection = "TEC 410";
            greenclass1.Faculty = "Mr. Darin Pearson";
            greenclass1.Semester = "Spring";
            greenclass1.StartTime = "10:30 am";
            greenclass1.StartTimeRaw = 30;
            greenclass1.EndTime = "11:20 am";
            greenclass1.EndTimeRaw = 20;
            greenclass1.Room = "Room VC 019";
            greenclass1.Course = "TEC 410";
            greenclass1.ClassName = "AP Computer Science A";
            greenclass1.EA7RecordsID = i;

            schedule.Add(greenclass1);

            ClassModel columbiaclass1 = new ClassModel();


            columbiaclass1.CycleDaysID = 11;
            columbiaclass1.BlockColor = "Columbia";
            columbiaclass1.BlockColorRGB = "9fddff";
            columbiaclass1.Day = "Monday";
            columbiaclass1.CourseSection = "SCI 540";
            columbiaclass1.Faculty = "Ms. Joan Tisdale";
            columbiaclass1.Semester = "Spring";
            columbiaclass1.StartTime = "11:25 am";
            columbiaclass1.StartTimeRaw = 25;
            columbiaclass1.EndTime = "12:55 pm";
            columbiaclass1.EndTimeRaw = 55;
            columbiaclass1.Room = "Room 355";
            columbiaclass1.Course = "SCI 540";
            columbiaclass1.ClassName = "AP Physics C Engineering Mechanics";
            columbiaclass1.EA7RecordsID = i;

            schedule.Add(columbiaclass1);

            ClassModel navyclass1 = new ClassModel();

            navyclass1.CycleDaysID = 11;
            navyclass1.BlockColor = "Navy";
            navyclass1.BlockColorRGB = "33339f";
            navyclass1.Day = "Monday";
            navyclass1.CourseSection = "ENG 410";
            navyclass1.Faculty = "Mrs. Lindsay Lumpkin";
            navyclass1.Semester = "Spring";
            navyclass1.StartTime = "1:00 pm";
            navyclass1.StartTimeRaw = 0;
            navyclass1.EndTime = "2:05 pm";
            navyclass1.EndTimeRaw = 5;
            navyclass1.Room = "Room 127";
            navyclass1.Course = "ENG 410";
            navyclass1.ClassName = "English 12";
            navyclass1.EA7RecordsID = i;

            schedule.Add(navyclass1);

            ClassModel purpleclass1 = new ClassModel();

            purpleclass1.CycleDaysID = 11;
            purpleclass1.BlockColor = "Purple";
            purpleclass1.BlockColorRGB = "800080";
            purpleclass1.Day = "Monday";
            purpleclass1.CourseSection = "BIB 420";
            purpleclass1.Faculty = "Mr. Lane Palmer";
            purpleclass1.Semester = "Spring";
            purpleclass1.StartTime = "2:10 pm";
            purpleclass1.StartTimeRaw = 10;
            purpleclass1.EndTime = "3:00 pm";
            purpleclass1.EndTimeRaw = 0;
            purpleclass1.Room = "Room 257";
            purpleclass1.Course = "BIB 520";
            purpleclass1.ClassName = "Senior Bible";
            purpleclass1.EA7RecordsID = i;

            schedule.Add(purpleclass1);

            ClassModel grayclass1 = new ClassModel();

            grayclass1.CycleDaysID = 11;
            grayclass1.BlockColor = "Gray";
            grayclass1.BlockColorRGB = "a9a9a9";
            grayclass1.Day = "Tuesday";
            grayclass1.CourseSection = "PRT - 8";
            grayclass1.Faculty = "";
            grayclass1.Semester = "Spring";
            grayclass1.StartTime = "8:05 am";
            grayclass1.StartTimeRaw = 5;
            grayclass1.EndTime = "8:55 am";
            greenclass1.EndTimeRaw = 55;
            grayclass1.Room = "";
            grayclass1.Course = "PRT";
            grayclass1.ClassName = "PRT";
            grayclass1.EA7RecordsID = i;

            schedule.Add(grayclass1);

            ClassModel redclass2 = new ClassModel();

            redclass2.CycleDaysID = 12;
            redclass2.BlockColor = "Red";
            redclass2.BlockColorRGB = "ff0000";
            redclass2.Day = "Tuesday";
            redclass2.CourseSection = "PRT - 1";
            redclass2.Faculty = "";
            redclass2.Semester = "Spring";
            redclass2.StartTime = "9:00 am";
            redclass2.StartTimeRaw = 0;
            redclass2.EndTime = "9:50 am";
            redclass2.EndTimeRaw = 50;
            redclass2.Room = "";
            redclass2.Course = "PRT";
            redclass2.ClassName = "PRT";
            redclass2.EA7RecordsID = i;

            schedule.Add(redclass2);

            ClassModel orangeclass2 = new ClassModel();

            orangeclass2.CycleDaysID = 12;
            orangeclass2.BlockColor = "Orange";
            orangeclass2.BlockColorRGB = "ffa500";
            orangeclass2.Day = "Tuesday";
            orangeclass2.CourseSection = "PRT - 2";
            orangeclass2.Faculty = "";
            orangeclass2.Semester = "Spring";
            orangeclass2.StartTime = "10:00 am";
            orangeclass2.StartTimeRaw = 0;
            orangeclass2.EndTime = "10:50 am";
            orangeclass2.EndTimeRaw = 50;
            orangeclass2.Room = "";
            orangeclass2.Course = "PRT";
            orangeclass2.ClassName = "PRT";
            orangeclass2.EA7RecordsID = i;

            schedule.Add(orangeclass2);

            ClassModel yellowclass2 = new ClassModel();

            yellowclass2.CycleDaysID = 12;
            yellowclass2.BlockColor = "Yellow";
            yellowclass2.BlockColorRGB = "ffff00";
            yellowclass2.Day = "Tuesday";
            yellowclass2.CourseSection = "MAT 520";
            yellowclass2.Faculty = "Mr. Gabe Anderson";
            yellowclass2.Semester = "Spring";
            yellowclass2.StartTime = "10:55 am";
            yellowclass2.StartTimeRaw = 55;
            yellowclass2.EndTime = "12:00 pm";
            yellowclass2.EndTimeRaw = 0;
            yellowclass2.Room = "Room 322";
            yellowclass2.Course = "MAT 520";
            yellowclass2.ClassName = "AP Calculus A/B";
            yellowclass2.EA7RecordsID = i;

            schedule.Add(yellowclass2);

            ClassModel greenclass2 = new ClassModel();

            greenclass2.CycleDaysID = 12;
            greenclass2.BlockColor = "Green";
            greenclass2.BlockColorRGB = "8000";
            greenclass2.Day = "Tuesday";
            greenclass2.CourseSection = "TEC 410";
            greenclass2.Faculty = "Mr. Darin Pearson";
            greenclass2.Semester = "Spring";
            greenclass2.StartTime = "1:00 pm";
            greenclass2.StartTimeRaw = 0;
            greenclass2.EndTime = "2:05 pm";
            greenclass2.EndTimeRaw = 5;
            greenclass2.Room = "Room VC 019";
            greenclass2.Course = "TEC 410";
            greenclass2.ClassName = "AP Computer Science A";
            greenclass2.EA7RecordsID = i;

            schedule.Add(greenclass2);

            ClassModel columbiaclass2 = new ClassModel();

            columbiaclass2.CycleDaysID = 12;
            columbiaclass2.BlockColor = "Columbia";
            columbiaclass2.BlockColorRGB = "9fddff";
            columbiaclass2.Day = "Tuesday";
            columbiaclass2.CourseSection = "SCI 540";
            columbiaclass2.Faculty = "Ms. Joan Tisdale";
            columbiaclass2.Semester = "Spring";
            columbiaclass2.StartTime = "2:10 pm";
            columbiaclass1.StartTimeRaw = 10;
            columbiaclass2.EndTime = "3:00 pm";
            columbiaclass2.EndTimeRaw = 0;
            columbiaclass2.Room = "Room 355";
            columbiaclass2.Course = "SCI 540";
            columbiaclass2.ClassName = "AP Physics C Engineering Mechanics";
            columbiaclass2.EA7RecordsID = i;

            schedule.Add(columbiaclass2);

            ClassModel navyclass2 = new ClassModel();

            navyclass2.CycleDaysID = 13;
            navyclass2.BlockColor = "Navy";
            navyclass2.BlockColorRGB = "33339f";
            navyclass2.Day = "Wednesday";
            navyclass2.CourseSection = "ENG 410";
            navyclass2.Faculty = "Mrs. Lindsay Lumpkin";
            navyclass2.Semester = "Spring";
            navyclass2.StartTime = "7:40 am";
            navyclass2.StartTimeRaw = 40;
            navyclass2.EndTime = "8:30 am";
            navyclass2.EndTimeRaw = 30;
            navyclass2.Room = "Room 127";
            navyclass2.Course = "ENG 410";
            navyclass2.ClassName = "English 12";
            navyclass2.EA7RecordsID = i;

            schedule.Add(navyclass2);

            ClassModel purpleclass2 = new ClassModel();

            purpleclass2.CycleDaysID = 13;
            purpleclass2.BlockColor = "Purple";
            purpleclass2.BlockColorRGB = "800080";
            purpleclass2.Day = "Wednesday";
            purpleclass2.CourseSection = "BIB 420";
            purpleclass2.Faculty = "Mr. Lane Palmer";
            purpleclass2.Semester = "Spring";
            purpleclass2.StartTime = "8:35 am";
            purpleclass2.StartTimeRaw = 35;
            purpleclass2.EndTime = "10:20 am";
            purpleclass2.EndTimeRaw = 20;
            purpleclass2.Room = "Room 257";
            purpleclass2.Course = "BIB 520";
            purpleclass2.ClassName = "Senior Bible";
            purpleclass2.EA7RecordsID = i;

            schedule.Add(purpleclass2);


            return schedule;

        }

        public List<StudentGradesModel> getStudentGrades()
        {
            List<StudentGradesModel> grades = new List<StudentGradesModel>();
            StudentGradesModel report;
            Random ran = new Random();

            int ea7 = ran.Next(222, 999);

            for (int i = 0; i < 7; i++)
            {
                report = new StudentGradesModel();

                //add attributes
                report.EA7RecordsID = ea7;
                report.CourseSection = ran.Next(1, 6).ToString();
                report.SectionCode = "F" + ran.Next(10000, 30000);
                
                switch(i)
                {
                    case 0:
                        report.Course = "MAT 410";
                        report.Classname = "Pre-Calculus";
                        report.Faculty = "Mrs. Shallenberger";
                        report.Room = "Room 346";
                        break;
                    case 1:
                        report.Course = "ENG 410";
                        report.Classname = "English 12";
                        report.Faculty = "Mr. Dixon";
                        report.Room = "Room 145";
                        break;
                    case 2:
                        report.Course = "BIB 420";
                        report.Classname = "Senior Bible";
                        report.Faculty = "Mr. Stone";
                        report.Room = "Room 257";
                        break;
                    case 3:
                        report.Course = "SCI 340";
                        report.Classname = "Anatomy & Physiology";
                        report.Faculty = "Mr. Kortz";
                        report.Room = "Room 340";
                        break;
                    case 4:
                        report.Course = "STU 120";
                        report.Classname = "Studio Recording I";
                        report.Faculty = "Mr. Magehee";
                        report.Room = "Room VC 230";
                        break;
                    case 5:
                        report.Course = "HIS 150";
                        report.Classname = "AP European History";
                        report.Faculty = "Dr. Davis";
                        report.Room = "Room 054";
                        break;
                    case 6:
                        report.Course = "PHO 410";
                        report.Classname = "Photography IV";
                        report.Faculty = "Mr. Von Qualen";
                        report.Room = "Room VC 124";
                        break;
                }

                report.Grade = (ran.Next(80, 100) + Math.Round(ran.NextDouble(), 3));

                //add to list
                grades.Add(report);
            }

            return grades;
        }

        public Model.AppData getPerson(PersonNameGenerator nameGen, PlaceNameGenerator placeGen, Random numGen, int EA7)
        {
            //create empty appdata
            Model.AppData appData = new Model.AppData();

            int num = numGen.Next(0, 2);

            if ((num % 2) == 0)
            {
                //generate female name data attributes
                appData.FirstName = nameGen.GenerateRandomFemaleFirstName();
                appData.MiddleName = nameGen.GenerateRandomFemaleFirstName();
                appData.Gender = "Female";
                appData.Title = "Ms.";
            }
            else
            {
                //generate male name data attributes
                appData.FirstName = nameGen.GenerateRandomMaleFirstName();
                appData.MiddleName = nameGen.GenerateRandomMaleFirstName();
                appData.Gender = "Male";
                appData.Title = "Mr.";
            }

            appData.LastName = nameGen.GenerateRandomLastName();
            appData.Siblings = nameGen.GenerateRandomFirstName() + " " + appData.LastName;
            appData.DisplayName = appData.FirstName + " " + appData.LastName;
            appData.NickName = appData.FirstName;
            appData.Faculty = appData.FirstName + " " + appData.LastName;
            appData.EA7RecordsID = EA7;

            //generate identifying information
            int yearS = numGen.Next(2015, 2019);
            int yearF = numGen.Next(2008, 2019);
            int id = numGen.Next(1111, 9999);
            int grade = 0;

            switch(yearS)
            {
                case 2015:
                    grade = 12;
                    break;
                case 2016:
                    grade = 11;
                    break;
                case 2017:
                    grade = 10;
                    break;
                case 2018:
                    grade = 9;
                    break;
            }

            int classOf = yearS + 3;

            appData.Email = appData.FirstName + "." + appData.LastName + "@govalor.com";
            appData.GroupEmailFaculty = "faculty@valorchristian.com";
            appData.GroupEmailStudent = "classof" + classOf.ToString() + "@govalor.com";
            appData.StudentIDNumber = yearS.ToString() + "S" + id.ToString();
            appData.SchoolIDForFacutly = yearF.ToString() + "F" + id.ToString();
            appData.FacultyID = id;
            appData.ClassOf = classOf;
            appData.GradeLevel = grade.ToString();
            appData.OrgUnitPathFaculty = "/Faculty-Staff";
            appData.OrgUnitPathStudent = "/Students/Class of " + classOf.ToString();

            //generate department
            List<string> departments = new List<string>();
            departments.Add("English");
            departments.Add("Math");
            departments.Add("Science");
            departments.Add("Arts");
            departments.Add("Academic Support");
            departments.Add("History");

            appData.Department = departments.ElementAt(numGen.Next(0, departments.Count()));

            //generate AD information
            if (grade == 10)
            {
                appData.RecordType = "Staff";
                appData.OUDefaultGroupFS = "Valor Staff";
                appData.Password = "12GoV@lor!";
            }
            else
            {
                appData.RecordType = "Faculty";
                appData.OUDefaultGroupFS = "Valor Faculty";
                appData.Password = "11G0V@alor!";
            }

            switch(grade)
            {
                case 9:
                    appData.OUClassGroupS = "Freshmen";
                    break;
                case 10:
                    appData.OUClassGroupS = "Sophomores";
                    break;
                case 11:
                    appData.OUClassGroupS = "Juniors";
                    break;
                case 12:
                    appData.OUClassGroupS = "Seniors";
                    break;
                default:
                    appData.OUClassGroupS = "Students"; //debugging purposes
                    break;
            }

            appData.OULocation = " ";
            appData.OUDefaultGroupS = "Valor Students";
            appData.OUClassGroupFS = "Faculty-Staff";


            //generate parent information
            int separated = numGen.Next(1, 4);

            if (separated == 3)
            {
                //first parents info
                appData.Parents1 = nameGen.GenerateRandomFemaleFirstAndLastName();
                appData.HomePhone1 = numGen.Next(222, 999).ToString() + "-" + numGen.Next(222, 999).ToString() + "-" + numGen.Next(2222, 9999).ToString();
                appData.AddressBlock1 = numGen.Next(2, 9999).ToString() + " " + placeGen.GenerateRandomPlaceName() + " St";
                appData.CityStatePostBlock1 = placeGen.GenerateRandomPlaceName() + ", CO " + numGen.Next(11111, 99999).ToString();

                //second parents info
                appData.Parents2 = nameGen.GenerateRandomMaleFirstName() + " " + appData.LastName;
                appData.HomePhone2 = numGen.Next(222, 999).ToString() + "-" + numGen.Next(222, 999).ToString() + "-" + numGen.Next(2222, 9999).ToString();
                appData.AddressBlock2 = numGen.Next(2, 9999).ToString() + " " + placeGen.GenerateRandomPlaceName() + " Ave";
                appData.CityStatePostBlock2 = placeGen.GenerateRandomPlaceName() + ", CO " + numGen.Next(11111, 99999).ToString();
            }
            else
            {
                //parents info
                appData.Parents1 = nameGen.GenerateRandomMaleFirstName() + " and " + nameGen.GenerateRandomFemaleFirstName() + " " + appData.LastName;
                appData.HomePhone1 = numGen.Next(222, 999).ToString() + "-" + numGen.Next(222, 999).ToString() + "-" + numGen.Next(2222, 9999).ToString();
                appData.AddressBlock1 = numGen.Next(2, 9999).ToString() + " " + placeGen.GenerateRandomPlaceName() + " Ave";
                appData.CityStatePostBlock1 = placeGen.GenerateRandomPlaceName() + ", CO " + numGen.Next(11111, 99999).ToString();

                //make empty so no null errors during post request
                appData.Parents2 = " ";
                appData.HomePhone2 = " ";
                appData.AddressBlock2 = " ";
                appData.CityStatePostBlock2 = " ";
            }

            return appData;
        }

        public List<FacultyAttendance.Record> getAttendanceCourses()
        {
            List<FacultyAttendance.Record> list = new List<FacultyAttendance.Record>();

            //reuse
            FacultyAttendance.Record course = new FacultyAttendance.Record();

            //Class #1
            FacultyAttendance.AppData math = new FacultyAttendance.AppData();
            math.CMCourse = "MAT 410";
            math.CMCourseCode = "C23896";
            math.CMBlockColor = "Magenta";
            math.CMSectionTitle = "Pre-Calculus";
            math.CMSectionCode = "S451";
            math.CMFaculty = "Ms. Johnson";
            math.CMFacultyID = "2013F4728";
            math.CMEmail = "Elizabeth.Johnson@govalor.com";
            course.appData = math;
            list.Add(course);

            //Class #2
            course = new FacultyAttendance.Record();
            FacultyAttendance.AppData english = new FacultyAttendance.AppData();
            english.CMCourse = "ENG 410";
            english.CMCourseCode = "C87342";
            english.CMBlockColor = "Yellow";
            english.CMSectionTitle = "English 12";
            english.CMSectionCode = "S867";
            english.CMFaculty = "Mr. Dixon";
            english.CMFacultyID = "2009F4690";
            english.CMEmail = "Jerry.Dixon@govalor.com";
            course.appData = english;
            list.Add(course);

            //Class #3
            course = new FacultyAttendance.Record();
            FacultyAttendance.AppData bible = new FacultyAttendance.AppData();
            bible.CMCourse = "BIB 420";
            bible.CMCourseCode = "C12263";
            bible.CMBlockColor = "Navy";
            bible.CMSectionTitle = "Senior Bible";
            bible.CMSectionCode = "S411";
            bible.CMFaculty = "Mr. Stone";
            bible.CMFacultyID = "2018F2387";
            bible.CMEmail = "Matt.Stone@govalor.com";
            course.appData = bible;
            list.Add(course);

            //Class #4
            course = new FacultyAttendance.Record();
            FacultyAttendance.AppData anat = new FacultyAttendance.AppData();
            anat.CMCourse = "SCI 340";
            anat.CMCourseCode = "C46193";
            anat.CMBlockColor = "Orange";
            anat.CMSectionTitle = "Anatomy & Physiology";
            anat.CMSectionCode = "S523";
            anat.CMFaculty = "Mr. Kortz";
            anat.CMFacultyID = "2015F5231";
            anat.CMEmail = "Anthony.Kortz@govalor.com";
            course.appData = anat;
            list.Add(course);

            //Class #5
            course = new FacultyAttendance.Record();
            FacultyAttendance.AppData hist = new FacultyAttendance.AppData();
            hist.CMCourse = "HIS 150";
            hist.CMCourseCode = "C52234";
            hist.CMBlockColor = "Gray";
            hist.CMSectionTitle = "AP European History";
            hist.CMSectionCode = "S429";
            hist.CMFaculty = "Dr. Davis";
            hist.CMFacultyID = "2008F0040";
            hist.CMEmail = "Robert.Davis@govalor.com";
            course.appData = hist;
            list.Add(course);

            //Class #6
            course = new FacultyAttendance.Record();
            FacultyAttendance.AppData studio = new FacultyAttendance.AppData();
            studio.CMCourse = "STU 120";
            studio.CMCourseCode = "C78234";
            studio.CMBlockColor = "Red";
            studio.CMSectionTitle = "Studio Recording I";
            studio.CMSectionCode = "S672";
            studio.CMFaculty = "Mr. Magehee";
            studio.CMFacultyID = "2012F4816";
            studio.CMEmail = "Jonathan.Magehee@govalor.com";
            course.appData = studio;
            list.Add(course);

            //Class #7
            course = new FacultyAttendance.Record();
            FacultyAttendance.AppData photo = new FacultyAttendance.AppData();
            photo.CMCourse = "PHO 410";
            photo.CMCourseCode = "C47628";
            photo.CMBlockColor = "Green";
            photo.CMSectionTitle = "Photography IV";
            photo.CMSectionCode = "S167";
            photo.CMFaculty = "Mr. Von Qualen";
            photo.CMFacultyID = "2008F7371";
            photo.CMEmail = "Michael.VonQualen@govalor.com";
            course.appData = photo;
            list.Add(course);

            return list;
        }

        public Model.Record getPermanentRecord(int EA7)
        {
            Model.Record jakeRecord = new Model.Record();
            Model.AppData appData = new Model.AppData();

            //create schedules
            List<ClassModel> studentSchedule = getStudentSchedule(1);
            List<ClassModel> teacherSchedule = getTeacherSchedule(1);
            List<StudentGradesModel> gradesReport = getStudentGrades();

            //add schedules
            appData.StudentSchedule = studentSchedule;
            appData.TeacherSchedule = teacherSchedule;
            appData.StudentGrades = gradesReport;

            //add rest of data
            appData.FirstName = "Jake";
            appData.MiddleName = "John";
            appData.LastName = "Vossen";
            appData.NickName = "Jake";
            appData.Title = "Mr.";
            appData.DisplayName = "Jake Vossen";
            appData.Gender = "Male";
            
            appData.Parents1 = "Jake and Pam Vossen";
            appData.HomePhone1 = "303-987-9876";
            appData.Parents2 = " ";
            appData.HomePhone2 = " ";
            appData.AddressBlock1 = "234 Mountain Dr";
            appData.CityStatePostBlock1 = "Highlands Ranch, Co 80234";
            appData.AddressBlock2 = " ";
            appData.CityStatePostBlock2 = " ";
            appData.Siblings = "Bryan Vossen";

            appData.StudentIDNumber = "2014S4538";
            appData.ClassOf = 2018;
            appData.GradeLevel = "12";
            appData.EA7RecordsID = EA7;
            appData.Email = "Jake.Vossen@govalor.com";
            appData.Password = "J@ke5P@ssw0rd!";

            appData.Faculty = " ";
            appData.Department = "Computer Science";
            appData.FacultyID = 4857;

            appData.SchoolIDForFacutly = "2012F2937";
            appData.OrgUnitPathFaculty = "/Faculty-Staff";
            appData.OrgUnitPathStudent = "/Students/Class of " + appData.ClassOf.ToString();
            appData.GroupEmailFaculty = "faculty@valorchristian.com";
            appData.GroupEmailStudent = "classof" + appData.ClassOf.ToString() + "@govalor.com";

            appData.OUClassGroupS = "Seniors";
            appData.OULocation = " ";
            appData.OUDefaultGroupS = "Valor Students";
            appData.OUClassGroupFS = "Faculty-Staff";
            appData.RecordType = " ";

            jakeRecord.appData = appData;
            
            return jakeRecord;
        }

        public void printPermanentRecord(Model.Record record)
        {
            //Print important attributes from each section
            Model.AppData print = record.appData;
            List<ClassModel> studentSched = print.StudentSchedule;
            List<StudentGradesModel> report = print.StudentGrades;

            Console.WriteLine("--------------Model with All Information--------------");
            Console.WriteLine("Name: " + print.FirstName + " " + print.LastName);
            Console.WriteLine("Parents: " + print.Parents1);
            Console.WriteLine("Student ID: " + print.StudentIDNumber);
            Console.WriteLine("Faculty ID: " + print.SchoolIDForFacutly);
            Console.WriteLine("Email: " + print.Email);
            Console.WriteLine("Faculty Group Email: " + print.GroupEmailFaculty);
            Console.WriteLine("Student Group Email: " + print.GroupEmailStudent);
            Console.WriteLine();
            Console.WriteLine("--Schedule--");
            Console.WriteLine("Monday Yellow Block: " + studentSched.ElementAt(2).ClassName + " " + studentSched.ElementAt(2).Faculty);
            Console.WriteLine("Monday Columbia Block: " + studentSched.ElementAt(5).ClassName + " " + studentSched.ElementAt(5).Faculty);
            Console.WriteLine();
            Console.WriteLine("--Eligibility Report--");
            Console.WriteLine(report.ElementAt(0).Classname + " " + report.ElementAt(0).Grade);
            Console.WriteLine(report.ElementAt(3).Classname + " " + report.ElementAt(3).Grade);
        }
    }
}
