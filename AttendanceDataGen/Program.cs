﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaModel;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;

namespace AttendanceDataGen
{
    class Program
    {
        public static void post(FacultyAttendance.RootObject root)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(root), ParameterType.RequestBody);

            // easy async support
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });
            Console.ReadLine();

            //client.Execute(request);
        }


        static void Main(string[] args)
        {
            GenerateRecord gen = new GenerateRecord();
            ModelTranslation translator = new ModelTranslation();

            Model.RootObject modelRoot = gen.getModel(1);
            FacultyAttendance.RootObject root = new FacultyAttendance.RootObject();
            root.records = modelRoot.records.ElementAt(0).appData.AttendanceCourses;

            post(root);
            
        }
    }
}
